import numpy as np
import matplotlib.pyplot as plt

from sklearn.model_selection import validation_curve
from sklearn.model_selection import learning_curve

from sklearn.metrics import precision_recall_curve


def plot_validation_curve(estimator, X, y, scoring, param_name, param_range,
                        title, cv=None, n_jobs=1, ylim=None, xlog=False, verbose=False):
    """http://scikit-learn.org/stable/auto_examples/model_selection/plot_validation_curve.html#sphx-glr-auto-examples-model-selection-plot-validation-curve-py"""
    
    train_scores, test_scores = validation_curve(estimator=estimator, X=X, y=y,
                                    param_name=param_name, param_range=param_range,
                                    scoring=scoring, cv=cv, n_jobs=n_jobs, verbose=verbose)

    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)

    if title is not None: plt.title(title)

    if ylim is not None: plt.ylim(*ylim)

    plt.xlabel(param_name)
    plt.ylabel("Score")

    lw = 2
    
    if xlog:
        plt.semilogx(param_range, train_scores_mean, 'o-', label="Training",
                color="r", lw=lw)
        plt.semilogx(param_range, test_scores_mean,  'o-', label="Cross-validation",
            color="g", lw=lw)
    else:
        plt.plot(param_range, train_scores_mean, 'o-', label="Training",
                color="r", lw=lw)
        plt.plot(param_range, test_scores_mean,  'o-', label="Cross-validation",
            color="g", lw=lw)

    plt.fill_between(param_range, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.1,
                     color="r")

    plt.fill_between(param_range, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color="g")

    plt.legend(loc="best")

    return plt

def plot_learning_curve(estimator, scoring, title, X, y, ylim=None, cv=None,
                        n_jobs=1, train_sizes=np.linspace(.1, 1.0, 5), verbose=False):
    """
    Generate a simple plot of the test and training learning curve.

    Parameters
    ----------
    estimator : object type that implements the "fit" and "predict" methods
        An object of that type which is cloned for each validation.

    title : string
        Title for the chart.

    X : array-like, shape (n_samples, n_features)
        Training vector, where n_samples is the number of samples and
        n_features is the number of features.

    y : array-like, shape (n_samples) or (n_samples, n_features), optional
        Target relative to X for classification or regression;
        None for unsupervised learning.

    ylim : tuple, shape (ymin, ymax), optional
        Defines minimum and maximum yvalues plotted.

    cv : int, cross-validation generator or an iterable, optional
        Determines the cross-validation splitting strategy.
        Possible inputs for cv are:
          - None, to use the default 3-fold cross-validation,
          - integer, to specify the number of folds.
          - An object to be used as a cross-validation generator.
          - An iterable yielding train/test splits.

        For integer/None inputs, if ``y`` is binary or multiclass,
        :class:`StratifiedKFold` used. If the estimator is not a classifier
        or if ``y`` is neither binary nor multiclass, :class:`KFold` is used.

        Refer :ref:`User Guide <cross_validation>` for the various
        cross-validators that can be used here.

    n_jobs : integer, optional
        Number of jobs to run in parallel (default 1).
    """

    if title is not None:
        plt.title(title)

    if ylim is not None:
        plt.ylim(*ylim)

    plt.xlabel("Training examples")
    plt.ylabel("Score")
    train_sizes, train_scores, test_scores = learning_curve(
        estimator, X, y, cv=cv, n_jobs=n_jobs, train_sizes=train_sizes, scoring=scoring, verbose=verbose)
    train_scores_mean = np.mean(train_scores, axis=1)
    train_scores_std = np.std(train_scores, axis=1)
    test_scores_mean = np.mean(test_scores, axis=1)
    test_scores_std = np.std(test_scores, axis=1)
    # plt.grid()

    plt.fill_between(train_sizes, train_scores_mean - train_scores_std,
                     train_scores_mean + train_scores_std, alpha=0.1,
                     color="r")
    plt.fill_between(train_sizes, test_scores_mean - test_scores_std,
                     test_scores_mean + test_scores_std, alpha=0.1, color="g")
    plt.plot(train_sizes, train_scores_mean, 'o-', color="r",
             label="Training")
    plt.plot(train_sizes, test_scores_mean, 'o-', color="g",
             label="Cross-validation")

    plt.legend(loc="best")

    return plt

from sklearn.metrics import precision_recall_curve

def plotPRC(clf, X, y, color, label=str()):
    yscore = clf.predict_proba(X)[:, 1]
#     average_precision = average_precision_score(y, yscore)
    precision, recall, _ = precision_recall_curve(y, yscore)
    
    plt.step(recall, precision, color=color, alpha=0.2,
         where='post', label=label+' PRC')
    plt.fill_between(recall, precision, step='post', alpha=0.2,
                     color=color)
    
    yhat = clf.predict(X)
    precision_p = precision_score(y, yhat)
    recall_p = recall_score(y, yhat)
    f1_p = f1_score(y, yhat)
    auroc_p = roc_auc_score(y, yscore)
    
    plt.scatter(recall_p, precision_p, color=color, linewidth=5, label=label + ' threshold=50%. \n' \
                                                    + 'Precision: {:0.2f}%'.format(precision_p*100) + '\n' \
                                                    + 'Recall: {:0.2f}%'.format(recall_p*100) + '\n' \
                                                    + 'F1: {:0.2f}%'.format(f1_p*100) + '\n' \
#                                                     + 'Average precision: {:0.2f}%'.format(average_precision*100) + '\n' \
                                                    + 'AUROC: {:0.2f}%'.format(auroc_p*100))

    return plt
    
def plotPerformance(clf, model_name, Xtrain, ytrain, Xtest, ytest, figsize=None, fontsize=None):
    if figsize is not None: plt.figure(figsize=figsize)
        
    plt.xlabel('Recall', fontsize=fontsize)
    plt.ylabel('Precision', fontsize=fontsize)
    plt.ylim([0.0, 1.05])
    plt.xlim([0.0, 1.0])
    plt.title('Performance of ' + model_name, fontsize=fontsize, fontweight='bold')

    plotPRC(clf, Xtrain, ytrain, color='b', label='In-sample')
    plotPRC(clf, Xtest, ytest, color='r', label='Out-of-sample')
    plt.legend()

    return plt

def GridSearch_table_plot(grid_clf, param_name, score_name,
                          num_results=15,
                          negative=True,
                          table=True,
                          graph=True,
                          display_all_params=True):

    '''Display grid search results
    https://www.kaggle.com/grfiv4/displaying-the-results-of-a-grid-search

    Arguments
    ---------

    grid_clf           the estimator resulting from a grid search
                       for example: grid_clf = GridSearchCV( ...

    param_name         a string with the name of the parameter being tested

    num_results        an integer indicating the number of results to display
                       Default: 15

    negative           boolean: should the sign of the score be reversed?
                       scoring = 'neg_log_loss', for instance
                       Default: True

    graph              boolean: should a graph be produced?
                       non-numeric parameters (True/False, None) don't graph well
                       Default: True

    display_all_params boolean: should we print out all of the parameters, not just the ones searched for?
                       Default: True

    Usage
    -----

    GridSearch_table_plot(grid_clf, "min_samples_leaf")

                          '''
    from matplotlib      import pyplot as plt
    from IPython.display import display
    import pandas as pd

    clf = grid_clf.best_estimator_
    clf_params = grid_clf.best_params_
    if negative:
        clf_score = -grid_clf.best_score_
    else:
        clf_score = grid_clf.best_score_
    clf_stdev = grid_clf.cv_results_['std_test_score'][grid_clf.best_index_]
    cv_results = grid_clf.cv_results_

    # print("best parameters: {}".format(clf_params))
    # print("best score:      {:0.5f} (+/-{:0.5f})".format(clf_score, clf_stdev))
    if display_all_params:
        import pprint
        pprint.pprint(clf.get_params())

    # pick out the best results
    # =========================
    scores_df = pd.DataFrame(cv_results).sort_values(by='rank_test_score')

    best_row = scores_df.iloc[0, :]
    if negative:
        best_mean = -best_row['mean_test_score']
    else:
        best_mean = best_row['mean_test_score']
    best_stdev = best_row['std_test_score']
    best_param = best_row['param_' + param_name]

    # display the top 'num_results' results
    # =====================================
    if table:
        display(pd.DataFrame(cv_results) \
                .sort_values(by='rank_test_score').head(num_results))

    # plot the results
    # ================
    scores_df = scores_df.sort_values(by='param_' + param_name)

    if negative:
        means = -scores_df['mean_test_score']
    else:
        means = scores_df['mean_test_score']
    stds = scores_df['std_test_score']
    params = scores_df['param_' + param_name]

    # plot
    if graph:
        plt.errorbar(params, means, yerr=stds)

        plt.axhline(y=best_mean + best_stdev, color='red')
        plt.axhline(y=best_mean - best_stdev, color='red')
        plt.plot(best_param, best_mean, 'or')

        plt.title(param_name + " vs " + score_name + "\nBest Score {:0.5f}".format(clf_score))
        plt.xlabel(param_name)
        plt.ylabel('Score')